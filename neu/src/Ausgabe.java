public class Ausgabe {

	public static void main(String[] args) {
		System.out.println("0 !	=		=   1");
		System.out.println("1 !	= 1		=   1");
		System.out.println("2 !	= 1*2		=   2");
		System.out.println("3 !	= 1*2*3		=   6");
		System.out.println("4 !	= 1*2*3*4	=  24");
		System.out.println("5 !	= 1*2*3*4*5	= 120");
		
		
		
	
		System.out.println("Fahrenheit    |   Celsius");
		System.out.println("-------------------------");
		System.out.println("-20           |    -28.89");
		System.out.println("-10           |    -23,33");
		System.out.println("+0            |    -17,78");
		System.out.println("+20           |     -6,67");
		System.out.println("30            |     -1.11");
	}

}
